using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioClienteBanco
{
    class Program
    {
        static void Main(string[] args)
        {
            //Clase GetySetOtroEjemplo
            var GetySetOtroEjemplo = new GetySetOtroEjemplo();
            GetySetOtroEjemplo.Apodo = "Ernesto";
            Console.WriteLine(GetySetOtroEjemplo.Apodo);
            Console.ReadKey();
        }
    }
}
namespace EjercicioClienteBanco
{
    public class GetySetOtroEjemplo
    {
        private string apodo;
        public string Apodo
        {

            get
            {
                return apodo;
            }
            set
            {
                if (value.Length <= 5)
                {
                    apodo = "Sin información";
                }
                else apodo = value;
            }

        }
    }
}

